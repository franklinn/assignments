This application has been implemented with orthogonality in mind. Its components are highly cohesive in terms of their responsibility and loosely coupled in relation to each other. A key benefit of this approach is design extensibility and ease of unit testing. 

Data storage is facilitated by the filesystem. MongoDB would have been a better option; however, the use case (single predicate of data persistence date in combination with requirement to query latest, previous and initial rates data etc) is such that a file based storage proved adequate. Open / Closed nature of design ensures that a MongoDB data storage implementation can be easily  applied. 

Exception handling is not explicit, rather a fail fast approach is used - e.g. application will fail if config file property is malformed. In the case of email sender client, code has been commented out to avert exception. 

Version of python used : 2.7
OS: Windows
IDE: PyDev. 
Repo: BitBucket Git 
Main script : FXPMgrMain.py 
Unit Test package : fxpmgr.test 